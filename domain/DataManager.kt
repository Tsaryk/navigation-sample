package navigation.test.navigation.basic.domain

import navigation.test.navigation.basic.data.Movie

class DataManager {

    fun getMovies(): List<Movie> = listOf(
        Movie("1", "Movie 1"),
        Movie("2", "Movie 2"),
        Movie("3", "Movie 3"),
        Movie("4", "Movie 4"),
        Movie("5", "Movie 5")
    )
}
