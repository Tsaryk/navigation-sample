package navigation.test.navigation.basic.ui.movie.list

import navigation.test.navigation.basic.data.Movie
import navigation.test.navigation.basic.ui.movie.details.MovieDetailsPreviewLauncher

/**
 * Movies list
 */
interface ListView {

    /**
     * Show movie
     */
    fun showMovies(movie: List<Movie>)

    /**
     * Show movie details
     */
    fun showMovieDetails(moviePreviewLauncher: MovieDetailsPreviewLauncher)
}
