package navigation.test.navigation.basic.ui.movie.list

import navigation.test.navigation.basic.data.Movie
import navigation.test.navigation.basic.domain.DataManager
import navigation.test.navigation.basic.ui.movie.details.MovieDetailsPreviewLauncher

/**
 * Simple presenter for imitate MVP. Just for demo navigation
 */
class MoviesPresenter {
    private val dataManager by lazy { DataManager() }
    private var viewState: ListView? = null

    /**
     * Attach view when it ready to show data
     */
    fun attach(viewState: ListView){
        this.viewState = viewState

        viewState.showMovies(dataManager.getMovies())
    }

    /**
     * Detach view
     */
    fun detach(){
        viewState = null
    }

    /**
     * Handle clicks on movie
     */
    fun onMovieClicked(movieId: String){
        viewState?.showMovieDetails(MovieDetailsPreviewLauncher(movieId))
    }
}
