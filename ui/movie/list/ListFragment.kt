package navigation.test.navigation.basic.ui.movie.list

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Button
import navigation.test.navigation.R
import navigation.test.navigation.basic.data.Movie
import navigation.test.navigation.basic.ui.movie.details.MovieDetailsFragment
import navigation.test.navigation.basic.ui.movie.details.MovieDetailsPreviewLauncher

class ListFragment: Fragment(), ListView {

    val presenter by lazy { MoviesPresenter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Button(context).setOnClickListener {
            presenter.onMovieClicked("2")
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.attach(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.detach()
    }

    override fun showMovies(movie: List<Movie>) {
        //show movies on UI
    }

    override fun showMovieDetails(moviePreviewLauncher: MovieDetailsPreviewLauncher) {
        fragmentManager?.beginTransaction()
            ?.replace(R.id.fragment_container, MovieDetailsFragment.newInstance(moviePreviewLauncher))
            ?.addToBackStack(null)
            ?.commit()
    }
}