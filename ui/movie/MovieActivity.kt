package navigation.test.navigation.basic.ui.movie

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import navigation.test.navigation.R
import navigation.test.navigation.basic.ui.movie.list.ListFragment

class MovieActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, ListFragment())
            .commit()
    }
}
