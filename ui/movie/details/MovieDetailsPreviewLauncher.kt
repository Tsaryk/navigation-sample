package navigation.test.navigation.basic.ui.movie.details

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class MovieDetailsPreviewLauncher(
    val id: String
) : Parcelable
