package navigation.test.navigation.basic.ui.movie.details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import navigation.test.navigation.basic.data.Movie
import navigation.test.navigation.basic.ui.movie.details.MovieDetailsPreviewLauncher
import java.lang.RuntimeException

class MovieDetailsFragment: Fragment() {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //get PreviewLauncher object
        val launcher = arguments?.getParcelable<MovieDetailsPreviewLauncher>(PREVIEW_LAUNCHER) ?: throw RuntimeException("PreviewLauncher is null")

        //do something else
    }

    companion object {
        private const val PREVIEW_LAUNCHER = "PREVIEW_LAUNCHER"

        fun newInstance(launcher: MovieDetailsPreviewLauncher) = MovieDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(PREVIEW_LAUNCHER, launcher)
            }
        }

    }

}
